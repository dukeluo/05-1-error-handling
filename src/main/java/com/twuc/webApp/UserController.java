package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
public class UserController {
    // 2.1
    @GetMapping("/api/errors/default")
    public void throwException() {
        throw new RuntimeException();
    }

    // 2.2
    @GetMapping("/api/errors/illegal-argument")
    public void getException() {
        throw new RuntimeException("This is a exception");
    }

    // 2.2
    @GetMapping("/api/exceptions/access-control-exception")
    public void getAccessControlException() {
        throw new AccessControlException("This is a access control exception");
    }

    // 2.2
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> exceptionHandler(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Something wrong with the argument");
    }

    // 2.2
    @ExceptionHandler(AccessControlException.class)
    public ResponseEntity<String> accessExceptionHandler(AccessControlException exception) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body("This is a access control handler");
    }

    // 2.3
    @GetMapping("/api/errors/null-pointer")
    public void getNullPointerException() {
        throw new NullPointerException();
    }

    // 2.3
    @GetMapping("/api/errors/arithmetic")
    public void getArithmeticException() {
        throw new ArithmeticException();
    }

    // 2.3
    @ExceptionHandler({ NullPointerException.class, ArithmeticException.class })
    public ResponseEntity<String> handleException() {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .body("Something wrong with the argument");
    }
}
