package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorController {

    // 2.4
    @GetMapping("/api/sister-errors/illegal-argument")
    public void getIllegalArgumentExceptionSister() {
        throw new IllegalArgumentException();
    }

    // 2.4
    @GetMapping("/api/brother-errors/illegal-argument")
    public void getIllegalArgumentExceptionBorder() {
        throw new IllegalArgumentException();
    }

}
