package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class UserControllerIntegrationTest {
    @Autowired
    TestRestTemplate testRestTemplate;

    // 2.1
    @Test
    void should_get_an_exception() {
        ResponseEntity<Void> entity =testRestTemplate.getForEntity("/api/errors/default", Void.class);

        assertEquals(500, entity.getStatusCodeValue());
    }

    // 2.2
    @Test
    void should_get_an_exception_with_specified_status_code() {
        ResponseEntity<String> entity =testRestTemplate.getForEntity("/api/errors/illegal-argument", String.class);

        assertEquals(500, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", entity.getBody().toString());
    }

    // 2.2
    @Test
    void should_get_an_access_control_exception_with_specified_status_code() {
        ResponseEntity<String> entity =testRestTemplate.getForEntity("/api/exceptions/access-control-exception", String.class);

        assertEquals(403, entity.getStatusCodeValue());
        assertNotEquals("Something wrong with the argument", entity.getBody().toString());
        assertEquals("This is a access control handler", entity.getBody().toString());
    }

    // 2.3
    @Test
    void should_get_an_null_pointer_exception() {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/errors/null-pointer", String.class);

        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", entity.getBody());
    }

    // 2.3
    @Test
    void should_get_an_arithmetic_exception() {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/errors/arithmetic", String.class);

        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", entity.getBody());
    }

    // 2.4

    @Test
    void should_handler_the_exception_by_the_global_controller() {
        ResponseEntity<String> entity;

        entity = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with brother or sister", entity.getBody());

        entity = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with brother or sister", entity.getBody());
    }
}